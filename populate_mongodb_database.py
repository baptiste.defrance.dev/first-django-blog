#!/usr/bin/env python3
"""
Script used to fill up the database so the site looks
functional upon first launch
"""
import uuid
import pymongo

from django.contrib.auth.hashers import make_password

from form import settings

if __name__ == "__main__":
    db_handle = pymongo.MongoClient(settings.DB_USERS)

    # Create some users
    dbname = db_handle["Users"]
    userdetails = dbname["userdetails"]

    users = [{"Username": "roger",
              "Password": make_password("waters", settings.PASSWORD_SALT),
              "Age": 80,
              "City": "London",
              "Email": "rogerwater@pinkfloyd.com"},
             {"Username": "david",
              "Password": make_password("gilmour", settings.PASSWORD_SALT),
              "Age": 77,
              "City": "London",
              "Email": "davidgilmour@pinkfloyd.com"}]

    for user in users:
        if list(userdetails.find({"Username": user["Username"]})):
            print("User already exists.")
            continue

        userdetails.insert_one(user)
        print(f'User {user["Username"]} created.')

    # Create some posts
    dbname = db_handle["Posts"]
    postdetails = dbname["postsdetails"]

    new_posts = [{"Date": "2023-10-15 09:45:31.679998",
                  "Title": "My bio",
                  "Content": "George Roger Waters (born 6 September 1943) is \
an English musician and singer-songwriter. In 1965, he co-founded the rock \
band Pink Floyd as the bassist. Following the departure of the songwriter, \
Syd Barrett, in 1968, Waters became Pink Floyd's lyricist, co-lead vocalist \
and conceptual leader until his departure in 1985. ",
                  "Username": "roger",
                  "Id": str(uuid.uuid4())},
                 {"Date": "2023-10-14 10:43:55.143344",
                  "Title": "Another Brick in the Wall, Part II",
                  "Content": "We don't need no education\n\
We don't need no thought control\n\
No dark sarcasm in the classroom\n\
Teacher, leave them kids alone\n\
Hey! Teacher! Leave them kids alone!",
                  "Username": "roger",
                  "Id": str(uuid.uuid4())},
                 {"Date": "2023-10-14 10:38:26.074402",
                  "Title": "My bio",
                  "Content": "David Jon Gilmour CBE (/ˈɡɪlmɔːr/ GHIL-mor; \
born 6 March 1946) is an English guitarist, singer and songwriter, who is a \
member of the rock band Pink Floyd. He joined as guitarist and co-lead \
vocalist in 1967, shortly before the departure of founding member Syd \
Barrett.[1] Pink Floyd achieved international success with the concept \
albums The Dark Side of the Moon (1973), Wish You Were Here (1975), Animals \
(1977), The Wall (1979) and The Final Cut (1983). By the early 1980s, they \
had become one of the highest-selling and most acclaimed acts in music history\
; by 2012, they had sold more than 250 million records worldwide, including \
75 million in the United States.[2] Following the departure of Roger Waters \
in 1985, Pink Floyd continued under Gilmour's leadership and released three \
more studio albums. ",
                  "Username": "david",
                  "Id": str(uuid.uuid4())},
                 {"Date": "2023-10-15 12:30:31.679998",
                  "Title": "Wish You Were Here",
                  "Content": "So, so you think you can tell\n\
Heaven from Hell? Blue skies from pain?\n\
Can you tell a green field from a cold steel rail?\n\
A smile from a veil? Do you think you can tell?\n\
Did they get you to trade your heroes for ghosts?\n\
Hot ashes for trees? Hot air for a cool breeze?\n\
Cold comfort for change? Did you exchange\n\
A walk-on part in the war for a lead role in a cage?",
                  "Username": "david",
                  "Id": str(uuid.uuid4())},
                 {"Date": "2023-10-15 09:45:31.679998",
                  "Title": "Breathe (In the Air)",
                  "Content": "Breathe, breathe in the air\n\
Don't be afraid to care\n\
Leave, but don't leave me\n\
Look around and choose your own ground\n\
\n\
For long you live and high you fly\n\
And smiles you'll give and tears you'll cry\n\
And all you touch and all you see\n\
Is all your life will ever be\n\
\n\
Run, rabbit, run\n\
Dig that hole, forget the Sun\n\
And when at last the work is done\n\
Don't sit down, it's time to dig another one\n\
\n\
For long, you live and high you fly\n\
But only if you ride the tide\n\
And balanced on the biggest wave\n\
You race towards an early grave",
                  "Username": "david",
                  "Id": str(uuid.uuid4())}]

    for post in new_posts:
        postdetails.insert_one(post)
        print(f'Post http://localhost:8000/post/{post["Id"]}/ created.')
