# First-Django-Blog

> Il est 13h55 à l'heure où j'écrit ce Readme (14/10/2023), j'ai passé environ 8h sur ce projet pour qu'il soit fonctionnel, cependant je risque d'y retoucher afin de paufiner et d'améliorer la lisibilité du code, je vais aussi peut être ajouter du CSS car ce n'est pas très joli pour le moment.       

C'est la première fois que j'utilise à la fois Django et MongoDB. C'était très fun!

## Premier projet de Django     

#### Ce projet une démo fonctionnelle de mini-blog codé en Django avec une BDD MongoDB.      
---

Il y a la possibilité de s'inscrire (`/register`), se connecter (`/login`), se déconnecter (`/logout`).

Il est possible d'accèder aux infos publiques des utilisateurs en allant sur `/account/{USERNAME}/`, si on est connecté avec le bon utilisateur, on peut:
- Modifier les infos de son compte (`/edit_account/`)
- Modifier son mot de passe (`/edit_piaf/`)
- Supprimer son compte (`/delete_account/`)
--- 

Une fois connecté il est possible d'écrire un nouveau post (`/new_post/`).       
Quand un post est créé il s'affiche sur la page d'accueil (`/`).

En cliquand sur le titre du post il est possible d'accèder au post complet ainsi qu'aux options de post:     
- Modification (`/edit_post/{UUID}`)
- Suppression (`/delete_post/{UUID}`)

### Execution     

Pour lancer le projet, rien de plus simple, il suffit d'executer:
```
docker-compose up -d # ou docker compose up -d 
```      
Pour y accèder:
```
http://localhost:8000/
```
Deux comptes sont disponibles (user:pass):
```
roger:waters
david:gilmour
```