"""
All the forms used by bapt's blog
"""

from django import forms


class LoginForm(forms.Form):
    """ Form used by the '/login' view
    """
    username = forms.CharField(label="Username", max_length=100)
    password = forms.CharField(label="Password",
                               widget=forms.PasswordInput(),
                               max_length=100)


class RegisterForm(forms.Form):
    """ Form used by the '/register' view
    """
    username = forms.CharField(label="Username", max_length=100)
    password = forms.CharField(label="Password",
                               max_length=100,
                               widget=forms.PasswordInput())
    age = forms.IntegerField()
    city = forms.CharField(label="City", max_length=100)
    email = forms.EmailField(label="Email", max_length=100)


class UpdateAccountForm(forms.Form):
    """ Form used by the '/register' view
    """
    age = forms.IntegerField()
    city = forms.CharField(label="City", max_length=100)
    email = forms.EmailField(label="Email", max_length=100)
    confirm = forms.BooleanField(label="Confirm")


class UpdatePasswordForm(forms.Form):
    """ Form used by the '/register' view
    """
    old_password = forms.CharField(label="Old Password",
                                   max_length=100,
                                   widget=forms.PasswordInput())
    new_password = forms.CharField(label="New Password",
                                   max_length=100,
                                   widget=forms.PasswordInput())


class NewPostForm(forms.Form):
    """ Form used to create a new post '/new_post'
        and also edit it '/edit_post'
    """
    title = forms.CharField(label="Title", max_length=100)
    content = forms.CharField(widget=forms.Textarea(), label="Content")


class ConfirmationForm(forms.Form):
    """ Form that lets us ask for confirmation before deleting
    """
    confirm = forms.BooleanField(label="Confirm")
