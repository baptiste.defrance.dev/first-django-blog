"""
Models for bapt's blog!
"""

from django.db import models


class BlogPost(models.Model):
    """ Model representing a Blog Post
    """
    title = models.CharField(max_length=100)
    content = models.CharField(max_length=500)
    date = models.CharField(max_length=100)
    username = models.CharField(max_length=100)
    ID = models.CharField(max_length=100)


class Account(models.Model):
    """ Model representing an account
    """
    username = models.CharField(max_length=100)
    age = models.CharField(max_length=10)
    city = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
