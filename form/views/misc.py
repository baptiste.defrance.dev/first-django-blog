"""
MISC VIEWS:
- home
"""
import pymongo

from django.shortcuts import render
from django.conf import settings

from form.models import BlogPost
from form.utils import quicksort_posts


# ---------------------------
# --- START OF MISC VIEWS ---
# ---------------------------

def home_view(request):
    """ HOME VIEW '/'
    """
    db_handle = pymongo.MongoClient(settings.DB_USERS)
    dbname = db_handle["Posts"]
    postdetails = dbname["postsdetails"]

    posts = quicksort_posts(list(postdetails.find({})))[::-1]
    nposts = []
    for post in posts:
        npost = BlogPost(title=post["Title"],
                         content=post["Content"],
                         date=post["Date"],
                         username=post["Username"],
                         ID=post["Id"])
        nposts.append(npost)

    return render(request, "home.html", {"posts": nposts})

# -------------------------
# --- END OF MISC VIEWS ---
# -------------------------
