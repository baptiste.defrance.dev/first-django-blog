"""
POST VIEWS:
    - /new_post
    - /post/{ID}
    - /delete_post/{ID}/
    - /edit_post/{ID}/
"""
import uuid
import pymongo

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.conf import settings
from django.utils import timezone
from django.contrib.auth.decorators import login_required

from form.models import BlogPost
from form.forms import NewPostForm
from form.forms import ConfirmationForm

# ---------------------------
# --- START OF POST VIEWS ---
# ------------------------------


def post_view(request, argument):
    """ POST VIEW '/post/{UUID}'
    """
    db_handle = pymongo.MongoClient(settings.DB_USERS)
    dbname = db_handle["Posts"]
    postdetails = dbname["postsdetails"]

    message = ""

    posts = list(postdetails.find({}))
    print(argument)
    curpost = None
    for post in posts:
        if post["Id"] == argument:
            curpost = BlogPost(title=post["Title"],
                               content=post["Content"],
                               date=post["Date"],
                               username=post["Username"],
                               ID=post["Id"])
            break

    if not curpost:
        message = "No post exists with this ID."

    return render(request, "posts/post.html", {"post": curpost,
                                               "message": message})


@login_required
def newpost_view(request):
    """ NEW POST VIEW '/new_post'
    """
    message = ''
    if request.method == "POST":
        form = NewPostForm(request.POST)
        if form.is_valid():
            db_handle = pymongo.MongoClient(settings.DB_USERS)
            dbname = db_handle["Posts"]
            postdetails = dbname["postsdetails"]

            title_given = request.POST.get('title')
            content_given = request.POST.get('content')

            if title_given and content_given:
                now = timezone.localtime(timezone.now())
                new_post = {"Date": f'{now.date()} {now.time()}',
                            "Title": title_given,
                            "Content": content_given,
                            "Username": request.user.username,
                            "Id": str(uuid.uuid4())}

                postdetails.insert_one(new_post)
                print("New post posted")
                message = f'Post was uploaded, click <a \
href=\"/post/{new_post["Id"]}\">here</a> to see the new post.'
            else:
                message = "Post was missing title or content and \
was not posted."
    else:
        form = NewPostForm()

    return render(request, "posts/newpost.html", {"form": form,
                                                  "message": message})


@login_required
def editpost_view(request, argument):
    """ EDIT POST VIEW '/edit_post/{UUID}'
    """
    db_handle = pymongo.MongoClient(settings.DB_USERS)
    dbname = db_handle["Posts"]
    postdetails = dbname["postsdetails"]

    message = ''

    curpost = list(postdetails.find({"Id": argument}))
    curpost_model = None
    if not curpost:
        message = "No post with this ID exists"
    else:
        curpost = curpost[0]
        curpost_model = BlogPost(title=curpost["Title"],
                                 content=curpost["Content"],
                                 date=curpost["Date"],
                                 username=curpost["Username"],
                                 ID=curpost["Id"])

    if request.method == "POST":
        # It means that the content or title has been updated
        form = NewPostForm(request.POST)
        if form.is_valid():
            title_given = request.POST.get('title')
            content_given = request.POST.get('content')

            if title_given and content_given:
                now = timezone.localtime(timezone.now())
                new_post = {"$set": {"Date": f'{now.date()} {now.time()}',
                                     "Title": title_given,
                                     "Content": content_given}}

                postdetails.update_one(curpost, new_post)
                print(f'Post {curpost["Id"]} updated')
                message = f'Post was updated, click <a href=\"/post/\
{curpost["Id"]}\">here</a> to see the new post'
            else:
                message = "Post was missing title or content and was \
not posted."
    else:
        form = NewPostForm(initial={"title": curpost["Title"],
                                    "content": curpost["Content"]})

    if request.user.username != curpost["Username"]:
        message = "You cannot edit this post"

    return render(request, "posts/editpost.html", {"form": form,
                                                   "message": message,
                                                   "post": curpost_model})


@login_required
def deletepost_view(request, argument):
    """ DELETE POST VIEW '/delete_post/{UUID}'
    """
    db_handle = pymongo.MongoClient(settings.DB_USERS)
    dbname = db_handle["Posts"]
    postdetails = dbname["postsdetails"]

    message = ''

    curpost = list(postdetails.find({"Id": argument}))
    curpost_model = None
    if not curpost:
        message = "No post with this ID exists"
    else:
        curpost = curpost[0]
        curpost_model = BlogPost(title=curpost["Title"],
                                 content=curpost["Content"],
                                 date=curpost["Date"],
                                 username=curpost["Username"],
                                 ID=curpost["Id"])

    if request.method == "POST":
        # It means that the content or title has been updated
        form = ConfirmationForm(request.POST)
        if form.is_valid():
            confirmation_given = request.POST.get('confirm')

            if confirmation_given:
                postdetails.delete_one(curpost)
                print(f'Post {curpost["Id"]} deleted')
                return HttpResponseRedirect("/")

            message = "Please check the box if you want to delete the post"
    else:
        form = ConfirmationForm()

    return render(request, "posts/deletepost.html", {"form": form,
                                                     "message": message,
                                                     "post": curpost_model})

# --------------------------
# --- END OF POSTS VIEWS ---
# --------------------------
