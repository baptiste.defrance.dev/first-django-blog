"""
ACCOUNT VIEWS:
    - login
    - register
    - account/{USERNAME}
    - edit_account
    - edit_password
    - delete_account
"""
import pymongo

from django.shortcuts import render
from django.http import HttpResponseRedirect

from django.conf import settings

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password

from form.models import Account
from form.forms import LoginForm, RegisterForm
from form.forms import ConfirmationForm, UpdateAccountForm, UpdatePasswordForm

# ------------------------------
# --- START OF ACCOUNT VIEWS ---
# ------------------------------


def register_view(request):
    """ ACCOUNT SIGN UP VIEW '/register'
    """
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            db_handle = pymongo.MongoClient(settings.DB_USERS)
            dbname = db_handle["Users"]
            userdetails = dbname["userdetails"]

            unhashed_pass = request.POST.get('password')
            user1 = {"Username": request.POST.get('username'),
                     "Password": make_password(unhashed_pass,
                                               settings.PASSWORD_SALT),
                     "Age": request.POST.get('age'),
                     "City": request.POST.get('city'),
                     "Email": request.POST.get('email')}

            if list(userdetails.find({"Username": user1["Username"]})):
                print("User already exists.")
                return HttpResponseRedirect("/register/")

            userdetails.insert_one(user1)
            print("User", user1["Username"], "created.")

            # Create user in django db
            if not User.objects.filter(username=user1["Username"]).first():
                user = User.objects.create_user(username=user1["Username"],
                                                email=user1["Email"],
                                                password=unhashed_pass)
                user.save()

            return HttpResponseRedirect("/login/")

    else:
        form = RegisterForm()

    return render(request, "login/register.html", {"form": form})


def login_view(request):
    """ LOGIN VIEW '/login'
    """
    message = ''
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            db_handle = pymongo.MongoClient(settings.DB_USERS)
            dbname = db_handle["Users"]
            userdetails = dbname["userdetails"]

            user_given = request.POST.get('username')
            pass_given = request.POST.get('password')

            user_obj = {"Username": user_given,
                        "Password": make_password(pass_given,
                                                  settings.PASSWORD_SALT)}

            if not list(userdetails.find(user_obj)):
                message = 'Bad Username/Password. Check if user is registered.'
            else:
                # Create the user in Django's SQLlite DB if it does not exist.
                # This lets me create users in mongodb manually
                if not User.objects.filter(username=user_given).first():
                    user = User.objects.create_user(username=user_given,
                                                    password=pass_given)
                    user.save()

                user = authenticate(username=user_given, password=pass_given)
                if user is not None:
                    login(request, user)

                    return HttpResponseRedirect("/")

                message = 'Bad login'
    else:
        form = LoginForm()

    return render(request, "login/login.html", {"form": form,
                                                "message": message})


def logout_view(request):
    """ LOGOUT VIEW '/logout'
    """
    logout(request)
    return HttpResponseRedirect("/")


def account_view(request, argument):
    """ ACCOUNT VIEW '/account/{USERNAME}'
    """
    db_handle = pymongo.MongoClient(settings.DB_USERS)
    dbname = db_handle["Users"]
    userdetails = dbname["userdetails"]

    message = ""

    user = list(userdetails.find({"Username": argument}))
    curuser = None
    if user:
        user = user[0]
        curuser = Account(username=user["Username"],
                          age=user["Age"],
                          city=user["City"],
                          email=user["Email"])
    else:
        message = "No user exists with this username."

    return render(request, "account/account.html", {"account": curuser,
                                                    "message": message})


@login_required
def editaccount_view(request):
    """ EDIT ACCOUNT VIEW '/edit_account'
    """
    db_handle = pymongo.MongoClient(settings.DB_USERS)
    dbname = db_handle["Users"]
    userdetails = dbname["userdetails"]

    message = ''

    curuser = list(userdetails.find({"Username": request.user.username}))
    if not curuser:
        message = "No user exists in the database with this username"
    else:
        curuser = curuser[0]

    if request.method == "POST":
        # It means that the content or title has been updated
        form = UpdateAccountForm(request.POST)
        if form.is_valid():
            email_given = request.POST.get('email')
            age_given = request.POST.get('age')
            city_given = request.POST.get('city')
            confirmation_given = request.POST.get('confirm')

            if confirmation_given:
                new_user = {"$set": {
                        "Email": email_given,
                        "Age": age_given,
                        "City": city_given
                    }}

                userdetails.update_one(curuser, new_user)
                print(f'User {curuser["Username"]} updated')
                return HttpResponseRedirect(f'/account/{curuser["Username"]}')
            message = "Check the box to change the account's information."
    else:
        form = UpdateAccountForm(initial={"age": curuser["Age"],
                                          "city": curuser["City"],
                                          "email": curuser["Email"]})

    if request.user.username != curuser["Username"]:
        message = "You cannot edit this user"

    return render(request, "account/editaccount.html", {"form": form,
                                                        "message": message})


@login_required
def editpassword_view(request):
    """ EDIT PASSWORD VIEW '/edit_piaf'
    """
    db_handle = pymongo.MongoClient(settings.DB_USERS)
    dbname = db_handle["Users"]
    userdetails = dbname["userdetails"]

    message = ''

    curuser = list(userdetails.find({"Username": request.user.username}))
    if not curuser:
        message = "No user exists in the database with this username"
    else:
        curuser = curuser[0]

    if request.method == "POST":
        # It means that the content or title has been updated
        form = UpdatePasswordForm(request.POST)
        if form.is_valid():
            oldpass_given = request.POST.get('old_password')
            newpass_given = request.POST.get('new_password')

            oldpass_hashed = make_password(oldpass_given,
                                           settings.PASSWORD_SALT)

            if oldpass_given and oldpass_hashed != curuser["Password"]:
                message = "Wrong password given. Nothing has changed"
                return render(request,
                              "account/editpassword.html",
                              {"form": form,
                               "message": message})

            if (oldpass_given and newpass_given and
                    oldpass_given != newpass_given):
                new_user = {"$set": {
                        "Password": make_password(newpass_given,
                                                  settings.PASSWORD_SALT)
                    }}

                userdetails.update_one(curuser, new_user)
                print(f'User {curuser["Username"]} updated in db')
                user = User.objects.get(username=curuser["Username"])
                user.set_password(newpass_given)
                user.save()
                return HttpResponseRedirect("/login")
            if oldpass_given == newpass_given:
                message = "The new password is the same as the old one. \
Nothing has changed."
    else:
        form = UpdatePasswordForm()

    if request.user.username != curuser["Username"]:
        message = "You cannot edit this user"

    return render(request, "account/editpassword.html", {"form": form,
                                                         "message": message})


@login_required
def deleteaccount_view(request):
    """ DELETE ACCOUNT VIEW '/delete_account'
    """
    db_handle = pymongo.MongoClient(settings.DB_USERS)
    dbname = db_handle["Users"]
    userdetails = dbname["userdetails"]

    message = ''

    curuser = list(userdetails.find({"Username": request.user.username}))
    if not curuser:
        message = "No user exists in the database with this username"
    else:
        curuser = curuser[0]

    if request.method == "POST":
        # It means that the content or title has been updated
        form = ConfirmationForm(request.POST)
        if form.is_valid():
            confirmation_given = request.POST.get('confirm')

            if confirmation_given:
                userdetails.delete_one(curuser)
                print(f'User {request.user.username} deleted from database')
                user = User.objects.get(username=request.user.username)
                user.delete()
                return HttpResponseRedirect("/")

            message = "Please check the box if you want to delete the user"
    else:
        form = ConfirmationForm()

    return render(request,
                  "account/deleteaccount.html",
                  {"form": form, "message": message})

# ------------------------------
# --- END OF ACCOUNT VIEWS ---
# ------------------------------
