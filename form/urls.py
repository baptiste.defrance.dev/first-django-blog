"""
URL configuration for form project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.conf import settings
from django.conf.urls.static import static

from .views import account, posts, misc

urlpatterns = [
    path('admin/', admin.site.urls),

    # MISC
    path('', misc.home_view),

    # POSTS
    path('new_post/', posts.newpost_view, name="new_post"),
    re_path(r"^post/([a-z0-9\-]+)/$", posts.post_view, name="view_post"),
    re_path(r"^edit_post/([a-z0-9\-]+)/$",
            posts.editpost_view,
            name="edit_post"),
    re_path(r"^delete_post/([a-z0-9\-]+)/$",
            posts.deletepost_view,
            name="delete_post"),

    # REGISTERING AND LOGGING IN/OUT
    path('register/', account.register_view, name="register"),
    path('login/', account.login_view, name="login"),
    path('logout/', account.logout_view, name="logout"),

    # ACCOUNT VIEW
    re_path(r"^account/([a-z0-9\-]+)/$", account.account_view, name="account"),
    path("edit_account/", account.editaccount_view, name="edit_account"),
    path("edit_piaf/", account.editpassword_view, name="edit_password"),
    path("delete_account/", account.deleteaccount_view, name="delete_account"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
