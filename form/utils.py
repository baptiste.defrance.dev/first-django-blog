#!/usr/bin/env python3
"""
Utilities, scripts, quick functions that may be needed
"""


def quicksort_posts(posts):
    """ Quicksort for post objects, post being a dict with this key:
        "Date"
    """
    lower = []
    equal = []
    greater = []

    if len(posts) > 1:
        pivot = posts[0]["Date"]
        for post in posts:
            if post["Date"] < pivot:
                lower.append(post)
            elif post["Date"] == pivot:
                equal.append(post)
            elif post["Date"] > pivot:
                greater.append(post)

        return quicksort_posts(lower) + equal + quicksort_posts(greater)

    return posts
