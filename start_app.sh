#!/bin/sh

echo "Populating database..."
DJANGO_SETTINGS_MODULE=form.settings python populate_mongodb_database.py

echo "Starting Django server"
python manage.py runserver 0.0.0.0:8000 

